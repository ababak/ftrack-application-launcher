
..
    :copyright: Copyright (c) 2021 ftrack

.. _release/release_notes:

*************
Release Notes
*************

.. release:: Upcoming

    .. change:: changed
        :tags: Setup

        Remove documentation dependencies from setup.py as already present in doc/requirements.txt

    .. change:: new
        :tags: Core

        Provide current "platform" as new event data.


    .. change:: new
        :tags: Core
        
        Allow configurations to be disabled through event.   

.. release:: 1.0.1
    :date: 2021-10-01


    .. change:: new
        :tags: Config

         Provide nuke-x configuration for pipeline integration. 


.. release:: 1.0.0
    :date: 2021-09-07

    .. change:: new

        First release version.
